## Windows

### Node and Watchman
-   Download the Windows installer from the [Nodes.js® web site](http://nodejs.org/).
-   Run and Follow the prompts in the installer.
-   Download the latest [watchman.zip](https://ci.appveyor.com/api/buildjobs/vkp4mmk1cri9jsel/artifacts/watchman.zip)
-   Extract the zip file and make sure that **watchman.exe** is located in a directory that is in your **PATH**.
-   Restart your computer.

### React Native CLI

Install React Native CLI globally.

```
npm install -g react-native-cli
```